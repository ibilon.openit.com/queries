import argparse
import dateutil
import pandas
import openit

def main():
    args   = get_args()
    data   = args.data
    output = args.output
    date   = args.date
    tz     = args.tz
    openit.data_dir = data
    df = get_data(date)
    df = set_datetime(df, tz)
    df = make_heatmap(df)
    save(df, output)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data')
    parser.add_argument('--output')
    parser.add_argument('--date', default='now')
    parser.add_argument('--tz',   default=dateutil.tz.tzlocal())
    return parser.parse_args()

def get_data(end):
    days       = 30
    periods    = days * 24
    freq       = 'H'
    date_range = pandas.date_range(end=end, periods=periods, freq=freq)
    return openit.data(date_range, datatype=102)

def set_datetime(df, tz):
    df['datetime'] = pandas.to_datetime(df['time'], unit='s').dt.tz_localize('UTC').dt.tz_convert(tz)
    return df

def save(df, filepath):
    df.to_csv(filepath, index=False)

def make_heatmap(df):
    df['hour'] = df['datetime'].dt.hour
    df['date'] = df['datetime'].dt.date
    df         = df[ ['date', 'hour', 'product', 'feature', 'feature_version', 'max_used'] ]
    return df

if __name__ == '__main__':
    main()
