import argparse
import dateutil
import pandas
import openit

def main():
    args   = get_args()
    data   = args.data
    output = args.output
    date   = args.date
    tz     = args.tz
    months = args.months
    openit.data_dir = data
    df = get_data(date, months)
    df = set_datetime(df, tz)
    df = make(df)
    save(df, output)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data')
    parser.add_argument('--output')
    parser.add_argument('--date',   default=last_month())
    parser.add_argument('--tz',     default=dateutil.tz.tzlocal())
    parser.add_argument('--months', default=1)
    return parser.parse_args()

def last_month():
    return pandas.to_datetime('now') - pandas.DateOffset(months=1)

def get_data(end, months):
    end        = pandas.to_datetime(end) + pandas.DateOffset(months=1)
    periods    = int(months)
    freq       = 'M'
    date_range = pandas.date_range(end=end, periods=periods, freq=freq)
    return openit.data(date_range, datatype=102)

def set_datetime(df, tz):
    df['datetime'] = pandas.to_datetime(df['time'], unit='s').dt.tz_localize('UTC').dt.tz_convert(tz)
    return df

def save(df, filepath):
    df.to_csv(filepath, index=False)

def make(df):
    df['date']     = df['datetime'].dt.date.max()
    df             = df[ ['date', 'product', 'feature', 'feature_version', 'max_used', 'duration'] ]
    df             = df[ df['max_used'] > 0 ]
    df             = df.groupby(['date', 'product', 'feature', 'feature_version', 'max_used']).sum().reset_index()
    df             = df.groupby(['date', 'product', 'feature', 'feature_version']).apply(group_fn)
    df['duration'] = df['duration'] / 3600
    return df

def group_fn(group):
    return group.transform(row_fn(group), axis=1)

def row_fn(group):
    def fn(row):
        row.duration = group[ group['max_used'] >= row.max_used ].sum().duration
        return row
    return fn

if __name__ == '__main__':
    main()
