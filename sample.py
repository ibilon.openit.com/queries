import argparse
import gzip

def main():
    args   = get_args()
    data   = args.data
    output = args.output
    with gzip.open(output, mode='wb') as file:
        file.write('name,age\n'.encode())
        file.write('foo,21\n'.encode())
        file.write('bar,22\n'.encode())

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data')
    parser.add_argument('--output')
    return parser.parse_args()

if __name__ == '__main__':
    main()
